:Program - PCM_XferCS

;$Header: /S56sr4/PCM/PCMServices/PCM_XferCS.txt 7     8/20/09 11:02a Cleclair $

//    Args: |0 = {IP|port} to CS listening job
//          |1 = |0 (U) {User Id
//               |1 (T) Template Mnemonic
//               |2 (Y) Editted Time (in secs)
//               |3 (S) Status
//               |4 (G) GUID
//               |5 (H) Hub Id
//               |6 (V) Visit Id
//               |7 (E) Encounter Data (if linked to an encounter)
//                      |0 Encounter#
//                      |1 Reason
//                      |2 Date
//                      |3 Time
//               |8 (P) Author Edit Request (only populated when an edit to the author is performed)
//                      |0 Provider Name
//                      |1 Provider MNE
//                      |2 Provider User ID (MT<>UNV<>MNE)
//               |9 (X) Actual Today/Now Timestamp (populated every time for all mpm documents)
//               |10 (D) Client Device
//               |11 (C) Is the editing mode that pdoc was in eg: EDM,CSEDIT,APR
//          |2 = IPC name of service launching this process
//          |3 = global o
//          |4 = trap flag
//          |5 = path to Template file
//          |6 = path to MTDD document
//          |7 = OPTIONAL: flag indicating an Amendment filing
//          |8 = OPTIONAL: path to MTDD file containing amendment data
//
// Note: Actual Timestamp (X) is only populated when the File time has been backdated, such as for Encounter Documents
//
// Returns: N/A
//
// Member 25 of set contains queries to send
//
// ToDo: add error/Okay information passing

:Code Main  (0)
@DA@LI@IF{@QV @{|0^N,|1^{U,T,Y,S,G,H,V,E,P,X,D,C},|2^I,|3^o,|4^t,|5@LE^F,|6@LE^M,|7^Z,|8@LE^J}},
1@Ik,
IF{@5^B,
   {"Ipc.dll","Tcp.dll"}@[@PD@QF]@&@NV "-COMERR Icp.dll/Tcp.dll do not exist"[@IF{B@NV @tt}];
   "Ipc.dll"@PD@Xi,
   "Tcp.dll"@PD@Xt,
   ("PCM_XferCS_",@JZ)@iI,I@iQ@NV "-COMERR Could not communicate with calling process"[@IF{B@NV @tt}];
   // B = error loading toolsets
   B;
   N[@LE@tt]@&@NV "IP or port of CS job is missing";
   Z="LVC" {U,T,N}@4;
   {U,T,Y,S,G,H,V}@{`User Id,Template Mnemonic,File Time,Status,Urn,Hub Id,Visit Id',}@TV@[@IF{|1@NV @(|0," MISSING ,");1 ""}]@PV~=@NL @CA~%2;
   IF{Z@NV IF{F@QF@NV "Template set does not exist";
              F@tg^A@NV "Unable to load the Template set";
              N@1@NV A@FS,"Failed opening network socket";
              "File template"[@tt]@tW,
              A@OU,
              {U,T,Y,S,G,H,V,"",IF{@F27@M27 "Y"},"",E|0,E|2,E|3,P|1,X,C}@3,
              ["patient id values sent"@tt]
              {D}@tW,
              ["additional patient id values sent"@tt]
              DO{@M25 @O25@K25@7,
                      @K25@tW,
                      @O25,
                      DO{@M25 @K25@7}@tW,
                      @C25@C25,
                      @N25}@tW,
              ["query values sent"@tt]
              M@tr@NV @DU,""@tW,"MTDD document not found";
              M@{,~#"\"}@%[@LE@("Filename sent: ",)@tt]@tW[@LE@("@tW returned ",@SV," bytes")@tt],
              DO{@MF 16384@Gn@tW}@tW,
              ["MTDD document sent"@tt]
              DO{@M19 @K19@tW}@tW,
              ["Providers sent"@tt],
              //  send levels of care for all providers
              // first break the text out into 65 character pieces, and move that to the
              // end of each provider's level of care list.
              @IL,
              DO{@M17 IF{@Q17 @{{{@R17,5}@$,
                                 {@R17,6}@~$,
                                 @R17|5@IF{@QV @[@{,65}@2]@MV;@{,65}@2}}@MV}@j17},
                      @N17}
              @v17@DL@U17,
              //
              // send the structure for each provider in the form:
              //   |0=provider
              //   |1=source
              //   |2=date
              //   |3=level mnemonic
              //   |4=level name
              //   |5=chosen vs recommended level structure
              //   |6=selected component values
              //   |7=Calculated values {History level calc|Exam level calc|{H CALC|ROS CALC|PFSH CALC|EXAM CALC}}
              //   |8=Highest Exam,
              //   |9 = @NL  (saved for future use)
              //   |10 = @NL  (saved for future use)
              //   |11 and on are all lines of the comment text.
              @F17
              DO{@M17 @O17
                      DO{@M17 @K17@tW}
                      @C17
                      @N17}@tW,
              ["Levels of Care sent"@tt],
              //{{{Section,Instance of Section},Action,{{Mnemonic}}}}
              IF{@M34 @V34@tW}""@tW
              ["Section Actions sent"@tt]
              // @V33 = Copy of global f|35|1 Saved in Documentation\Codesets\Document.txt by "IF{f|35|1 @u33}"
              IF{@F33@R33 @V33@tW}""@tW
              "Encounter Information send"@tt,
              //
              {"+OK",@tR^R[@("Read at close: ",@LE)@tt]@tC|0$3}@~=[@Cr] @DU,"Server did not respond";
              @NL@U17@U34,
              {@QU@CU,F}@tp,
              1 ""};
      Z="SA" IF{J@QF@NV "Amendment text file does not exist";
                N@1@NV "Failed opening network socket";
                "File amendment sections"[@tt]@tW,
                {U,T,Y,S,G,H,V,"Y","","",E|0,E|2,E|3,P|1,X,C}@3,
                ["patient id values sent"@tt]
                {D}@tW,
                ["additional patient id values sent"@tt]
                J@tr@NV @DU,""@tW,"MTDD amendment file not found";
                J@{,~#"\"}@%[@LE@("Filename sent: ",)@tt]@tW
                [@LE@("@tW returned ",@SV," bytes")@tt],
                DO{@MF 16384@Gn@tW}@tW,
                ["MTDD amendment file sent"@tt]
                F@QF@NV "Template set does not exist";
                F@tg^A@NV "Unable to load the Template set";
                A@OU,
                DO{@M25 @O25@K25@tW,
                        @K25@tW,
                        @O25,
                        DO{@M25 @K25@tW}@tW,
                        @C25@C25,
                        @N25}@tW,
                ["query values sent"@tt],
                @DU,
                {"+OK",@tR^R[@("Read at close: ",@LE)@tt]@tC|0$3}@~=[@Cr] "Server did not respond";
                1 ""};
      IF{J@QF@NV "Amendment text file does not exist";
         N@1@NV "Failed opening network socket";
         "File amendment"[@tt]@tW,
         {U,T,Y,S,G,H,V,"Y","","",E|0,E|2,E|3,P|1,X,C}@3,
         ["patient id values sent"@tt]
         {D}@tW,
         ["additional patient id values sent"@tt]
         J@tr@NV @DU,""@tW,"MTDD amendment file not found";
         J@{,~#"\"}@%[@LE@("Filename sent: ",)@tt]@tW
         [@LE@("@tW returned ",@SV," bytes")@tt],
         DO{@MF 16384@Gn@tW}@tW,
         ["MTDD amendment file sent"@tt]
         {"+OK",@tR^R[@("Read at close: ",@LE)@tt]@tC|0$3}@~=[@Cr] "Server did not respond";
         1 ""}}}
@IF{@{"-COMERR",$7}@= [@IF{B@NV @tt}];
    @IF{@NV "+OK";
        1 @("-ERR PCM_XferCS: ",)}@{I,@{,R|1}}@iW,DO{IF{@RK=258 @CK,1;
                                                        @EK @CK,1;
                                                        @LK=308 @iR^Q,""}},Q};

//- open network connection (arg: {IP or server name|port})
:Code OpenNetworkConnection  (1)
^{N,P}
IF{N@QA@[@IF{="." "|"}]@CA@("{",,"}")@LI@[<256]@PV@CV~=4@NV N^I;
   N@AN^I},
DO{IF{{I,P}[@tD]@tO^B@NV C+1<6^C}}
IF{B [600@tH]};

// break string at spaces {string,max length,{split chars}}
:Code 2 BreakStringAtSpaces  (2)
@{|0@QA@IU@U0,|1^A,|2@IF{@NV ` ';@QV;@{}}@U1}
IF{@V0@CV~>A {@V0@CA};
   @V0@[@F1@Y1]@U2,DO{@M2 IF{@R2 @H2@W2},@N2},@V2@PV@U2,@L0@H0@J2,
   A^B,@F2,
   DO{IF{@A2 @N2@R2@P2<B} @N2;
      @M2 @K2@J3+A+1^B},
   @F0@F3,
   DO{@M3 @NL@N4@I4@O4,
          DO{@H0~>@R3 @K0@J4},
          @C4,@N3},
   @V4@[@CA@SA]}@DU;

// |0  (U) {User Id
// |1  (T) Template Mnemonic
// |2  (Y) Editted Time (in secs)
// |3  (S) Status
// |4  (G) GUID
// |5  (H) Hub Id
// |6  (V) Visit Id
// |7      Ammendments[YN]
// |8      "Y" IF signing NOT allowed due to required components not being filled in
// |9      Reserved for CS Queue File time
// |10     Encounter#                  (if linked to an encounter)
// |11     EncounterDate (YYYYMMDD)    (if linked to an encounter)
// |12     EncounterTime (HHMM)        (if linked to an encounter)
// |13     Provider MNE                (only populated when an edit to the author is performed)
// |14 (X) Actual Today/Now Timestamp  (populated every time for all mpm documents)
:Code 3 - Send Header to CS
@tW;

// |0=user id
// |1=lvc structure
// |2=network id
:Code 4 - Send HandHeld care level data to CS
^{U,T,N},
IF{N@1@NV "Failed opening network socket";
   "File LVC"[@tt]@tW,
   // @tW = {hubid|visitid|date|time|duration|""|user|encounter|carelevel|
   //        history|exam|mdm|pri.diag|sec.diag|procedure}
   T@{|0,|1|1,|2|0,|2|1,|3,|4,|5,|6|0,|7|0,|8,|9,|10,|11|0,|12|0,|13}@tW,
   ["LVC sent"@tt]
   {"+OK",@tR^R[@("Read at close: ",@LE)@tt]@tC|0$3}@~= "Server did not respond";
   1 ""};

:Code 5 - Load PCMServices Tools
IF{"PCMDictTools.mts"@PD@QF^A@NV "-ERR Cannot find 'PCMDictTools.mts'";
   A@GF^A@NV "-ERR Cannot load 'PCMDictTools.mts'";
   A@Yt,""};

:Code 6 - Unused
//// ::Unused::

// Escape char delimited working value
// If pointer past size, send remainder as internal and exit
// If end in escape char move forward
// Send chunk as internal and process remainder
// Reset pointer to base size
:Code 7 - Send data as chunks
[16000^B^P]
@EE,
@DO{@SV@{~=0,@{,P}@<}@& @EI@tW,"";
    @{,P}@$%1=(27@SI) [P+1^P];
    @{,P}@$ [@{,P}@$@EI@tW]@{,P}@~$
            [B^P]};